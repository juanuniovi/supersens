/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#define SD_SPI_HANDLE hspi2
#define MAXDATAROW 10000  //Max writing columns per file

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi2;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI2_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/*Function for the initialization of LSM6DS3*/
void Lsm6ds3_Init(void)
{
	//Variables
	uint8_t check=0;
	uint8_t Data;
	HAL_StatusTypeDef ret;


	ret = HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R  ,LSM6DS3_WHO_AM_I_REG ,1, &check, 1, 1000);   // check device ID through reading WHO_AM_I

	if (check == 106)  // 106 will be returned by the sensor if everything goes well
	{
		Data = 0x4C;	//01001100   Gyroscope at 104Hz, 2000 dps and bypassmode
		ret = HAL_I2C_Mem_Write(&hi2c1, LSM6DS3_ADDRESS_W  , LSM6DS3_CTRL2_G, 1,&Data, 1, 10);

		Data = 0x4A;   // 01001010 linear accelerometer at 104Hz, +-4g bypass mode and a low pass filter with 100Hz
		ret = HAL_I2C_Mem_Write(&hi2c1, LSM6DS3_ADDRESS_W  , LSM6DS3_CTRL1_XL, 1, &Data, 1, 10);

		Data = 0x00;  // set gyroscope power mode to high performance and bandwidth to 16 MHz
		HAL_I2C_Mem_Write(&hi2c1, LSM6DS3_ADDRESS_W  , LSM6DS3_CTRL7_G, 1, &Data, 1, 10);

		Data = 0x00;  // 00001001 ODR config register to ODR/4
		HAL_I2C_Mem_Write(&hi2c1, LSM6DS3_ADDRESS_W  , LSM6DS3_CTRL8_XL, 1, &Data, 1, 10);
		//
	}
}

/*Function for reading accelerometer*/
void Lsm6ds3_Read_Accel (void)
{
	uint8_t Rec_Data[6] = {0, 0, 0, 0, 0, 0};
	int16_t Axx = 0;
	int16_t Ayy = 0;
	int16_t Azz = 0;

	/*
	HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUTX_L_XL, 1, Rec_Data[0], 6, 10);  //Read the low x-axis of accelerometer
	HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUTX_H_XL, 1, Rec_Data[1], 1, 10);  //Read the high x-axis of accelerometer

	HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUTY_L_XL, 1, Rec_Data[2], 1, 10);  //Read the low y-axis of accelerometer
	HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUTY_H_XL, 1, Rec_Data[3], 1, 10);  //Read the high y-axis of accelerometer

	HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUTZ_L_XL, 1, Rec_Data[4], 1, 10);  //Read the low z-axis of accelerometer
	HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUTZ_H_XL, 1, Rec_Data[5], 1, 10);  //Read the high z-axis of accelerometer
*/
	HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUTX_L_XL, 1, Rec_Data, 6, 1000);

	Accel_X_RAW = (int16_t)(Rec_Data[1] << 8 | Rec_Data [0]);
	Accel_Y_RAW = (int16_t)(Rec_Data[3] << 8 | Rec_Data [2]);
	Accel_Z_RAW = (int16_t)(Rec_Data[5] << 8 | Rec_Data [4]);


	/*** convert the RAW values into acceleration in 'g'
	     we have to divide according to the Full scale value set in FS_SEL
	     I have configured FS_SEL = 0. So I am dividing by 16384.0
	     for more details check ACCEL_CONFIG Register   ((float_t)lsb * 70.0f);           ****/

	Ax = Accel_X_RAW; //*((float)Accel_Y_RAW  * 122.0f / 1000.0f);
	Ay = Accel_Y_RAW;
	Az = Accel_Z_RAW;

	Axx = Ax;  // Variables to read in the debug
	Ayy = Ay;
	Azz = Az;

}

/*Function for reading gyroscope*/
void Lsm6ds3_Read_Gyro (void)
{
	uint8_t Rec_Data[6] = {0, 0, 0, 0, 0, 0};
	int16_t Gxx = 0;
	int16_t Gyy = 0;
	int16_t Gzz = 0;

	/*
	HAL_I2C_Mem_Read (&hi2c1 , LSM6DS3_ADDRESS_R , LSM6DS3_OUTX_L_G, 1 , Rec_Data[0] , 1 , 10);    //Read the low x-axis of gyroscope
	HAL_I2C_Mem_Read (&hi2c1 , LSM6DS3_ADDRESS_R , LSM6DS3_OUTX_H_G, 1 , Rec_Data[1] , 1 , 10);    //Read the high x-axis of gyroscope

	HAL_I2C_Mem_Read (&hi2c1 , LSM6DS3_ADDRESS_R , LSM6DS3_OUTY_L_G, 1 , Rec_Data[2] , 1 , 10);    //Read the low y-axis of gyroscope
	HAL_I2C_Mem_Read (&hi2c1 , LSM6DS3_ADDRESS_R , LSM6DS3_OUTY_H_G, 1 , Rec_Data[3] , 1 , 10);    //Read the high y-axis of gyroscope

	HAL_I2C_Mem_Read (&hi2c1 , LSM6DS3_ADDRESS_R , LSM6DS3_OUTZ_L_G, 1 , Rec_Data[4] , 1 , 10);    //Read the low z-axis of gyroscope
	HAL_I2C_Mem_Read (&hi2c1 , LSM6DS3_ADDRESS_R , LSM6DS3_OUTZ_H_G, 1 , Rec_Data[5] , 1 , 10);    //Read the high z-axis of gyroscope
*/
	HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUTX_L_G, 1, Rec_Data, 6, 1000);

	Gyro_X_RAW = (int16_t)(Rec_Data[1] << 8 | Rec_Data [0]);
	Gyro_Y_RAW = (int16_t)(Rec_Data[3] << 8 | Rec_Data [2]);
	Gyro_Z_RAW = (int16_t)(Rec_Data[5] << 8 | Rec_Data [4]);


	/* convert the RAW values into dps (�/s)
	     we have to divide according to the Full scale value set in FS_SEL
	     I have configured FS_SEL = 0. So I am dividing by 131.0
	     for more details check GYRO_CONFIG Register  ((float_t)lsb * 122.0f / 1000.0f); */

	Gx = Gyro_X_RAW; // x value converted ((float)Gyro_X_RAW * 70.0f);
	Gy = Gyro_Y_RAW; // y value converted
	Gz = Gyro_Z_RAW; // z value converted *(70/1000)


	Gxx = Gx;
	Gyy = Gy;   // Variables to read in the debug
	Gzz = Gz;

}
/*Function for reading temperature*/
void Lsm6ds3_Read_Temp(void)
{
	uint8_t Rec_Data[2] = {0, 0};

		HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUT_TEMP_L, 1, Rec_Data, 2, 10);    //Read the low temperature
		//HAL_I2C_Mem_Read (&hi2c1, LSM6DS3_ADDRESS_R , LSM6DS3_OUT_TEMP_H, 1, Rec_Data[1], 1, 10);    //Read the high temperature

		Temperature = (int16_t)(Rec_Data[1] << 8 | Rec_Data [0]);
		Temp = Temperature;  //((float)Temperature / 16.0f + 25.0f);
	//7emperature = Temperature/125.0;
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_SPI2_Init();
  MX_USART2_UART_Init();
  MX_FATFS_Init();

  /* USER CODE BEGIN 2 */
  //some variables for FatFs
    FATFS FatFs; 	       //Fatfs handle
    FIL filResulOut;       //File handle
    FRESULT ferror;        //Result after operations
    //char buffer [1024];  //Store data
    int x=0;
    int i=0;
	char nombre[20];
	char texto = ".txt";
/*
    //Open the file system
     ferror = f_mount(&FatFs, "", 0);
     //ferror = filenameGenerator(&ui32_filename,&c_filename)*/  //Esta funcion genera el nombre del archivo   ------------------------------> TODO: comprobar & en el puntero

     //ferror = f_open(&filResulOut, "Lectura2.txt" ,  FA_OPEN_ALWAYS |FA_READ| FA_WRITE);  //Create a writable txt file call dummy

     //f_printf(&filResulOut, "Ax\t Ay\t Az\t Gx\t Gy\t Gz\t Temp\t \n \n");

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  Lsm6ds3_Init();
	  for (i=0; i<20; i++){
	  		  //Open the file system
	  		  ferror = f_mount(&FatFs, "", 0);
	  		  sprintf(nombre, "Lectura%d", i);
	  		  strcat(nombre, ".txt");
	  		  ferror = f_open(&filResulOut, nombre ,  FA_OPEN_ALWAYS |FA_READ| FA_WRITE);  //Create a writable txt file
	  		  f_printf(&filResulOut, "Ax\t Ay\t Az\t Gx\t Gy\t Gz\t Temp\t \n \n");
	  		  x = 0;
	  		  while (x<2000){

	  		      Temp=0;
	  			  Ax=0;
	  			  Ay=0;
	  			  Az=0;
	  			  Gx=0;
	  	     	  Gy=0;
	  			  Gz=0;
	  			  Lsm6ds3_Read_Accel();
	  			  Lsm6ds3_Read_Gyro();
	  		 	  Lsm6ds3_Read_Temp();

	  			  HAL_Delay(10);

	  			  if (ferror == FR_OK) {

	  				  f_printf(&filResulOut, "%d\t %d\t %d\t %d\t %d\t %d\t %d\t \n",Ax,Ay,Az,Gx,Gy,Gz,Temp );

	  			  }
	  			  x++;
	  		  }
	  		  if (x == 2000){
	  			  // Close the file
	  			  f_close(&filResulOut);
	  		  }
	  		  if (i == 20){
	  			  break;
	  		  }
	  	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_128;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 7;
  hspi2.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi2.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC13 */
  GPIO_InitStruct.Pin = GPIO_PIN_13;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : PA5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB1 */
  GPIO_InitStruct.Pin = GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

