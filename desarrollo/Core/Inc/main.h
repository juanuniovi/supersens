/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

extern I2C_HandleTypeDef hi2c1;
/*Temperature sensor output data (16 bits)*/

#define LSM6DS3_ADDRESS_R          0xD5       //11010101 Address when you have to read
#define LSM6DS3_ADDRESS_W          0xD4       //11010100 Address when you have to write

#define LSM6DS3_WHO_AM_I_REG       0X0F
#define LSM6DS3_CTRL1_XL           0X10
#define LSM6DS3_CTRL2_G            0X11

#define LSM6DS3_STATUS_REG         0X1E

#define LSM6DS3_CTRL6_C            0X15
#define LSM6DS3_CTRL7_G            0X16
#define LSM6DS3_CTRL8_XL           0X17

/*Temperature sensor output data (16 bits))*/
#define LSM6DS3_OUT_TEMP_L                         0x20
#define LSM6DS3_OUT_TEMP_H                         0x21

/*Angular rate sensor pitch axis x (16 bits)*/  //Eje de paso del sensor de velocidad angular (x)
#define LSM6DS3_OUTX_L_G                           0x22
#define LSM6DS3_OUTX_H_G                           0x23

/*Angular rate sensor roll axis y (16 bits)*/    //Eje de balanceo del sensor de velocidad angular (y)
#define LSM6DS3_OUTY_L_G                           0x24
#define LSM6DS3_OUTY_H_G                           0x25

/*Angular rate sensor yaw axis z (16 bits)*/      //Eje de gui�ada/direccion del sensor de velocidad (x)
#define LSM6DS3_OUTZ_L_G                           0x26
#define LSM6DS3_OUTZ_H_G                           0x27

/*Linear acceleration sensor X-axis output register (16 bits)*/
#define LSM6DS3_OUTX_L_XL                          0x28
#define LSM6DS3_OUTX_H_XL                          0x29

/*Linear acceleration sensor Y-axis output register (16 bits)*/
#define LSM6DS3_OUTY_L_XL                          0x2A
#define LSM6DS3_OUTY_H_XL                          0x2B

/*Linear acceleration sensor Z-axis output register (16 bits)*/
#define LSM6DS3_OUTZ_L_XL                          0x2C
#define LSM6DS3_OUTZ_H_XL                          0x2D

int16_t Accel_X_RAW;
int16_t Accel_Y_RAW;
int16_t Accel_Z_RAW;

int16_t Gyro_X_RAW;
int16_t Gyro_Y_RAW;
int16_t Gyro_Z_RAW;
int16_t Temperature;

int16_t Ax;
int16_t Ay;
int16_t Az;
int16_t Gx;
int16_t Gy;
int16_t Gz;
int16_t Temp;

HAL_StatusTypeDef Imu_readTemperature(uint8_t address, uint8_t pinCfg, uint16_t *temperature);

void Lsm6ds3_Init(void);
void Lsm6ds3_Read_Accel (void);
void Lsm6ds3_Read_Gyro (void);
void Lsm6ds3_Read_Temp(void);

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SD_CS_Pin GPIO_PIN_1
#define SD_CS_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define SD_SPI_HANDLE hspi2

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
