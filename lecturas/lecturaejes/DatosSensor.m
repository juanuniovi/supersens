% Se carga el archivo de texto en matlab
A = importdata('LecturaSensor2.txt');


% Las primeras tres columnas son datos del acelerómetro
Axx=A(:,1);    % Primera columna, eje x
Ayy=A(:,2);    % Segunda columna, eje y
Azz=A(:,3);    % Tercera columna, eje z


% Los datos llegan con un poco de ruido, con lo que lo mejor
% es realizarles un filtro
Ax = medfilt1(Axx,50);
Ay = medfilt1(Ayy,50);
Az = medfilt1(Azz,50);

%Tras ello, se grafican los valores de los tres ejes
figure(1)
plot(Ax)    %Eje x
hold on
plot(Ay)    %Eje y
hold on
plot(Az)    %Eje z
hold on
yline(-9.8)   %Linea de referencia en -9.8 para comparar
hold on
yline(9.8)   %Tambien en 9.8 por si es positiva
hold on
legend('Ax','Ay','Az')


% Ahora se hace lo mismo con el giroscopio
% Las primeras tres columnas son datos del giroscopio

Gxx=A(:,4);        % Cuarta columna, eje x
Gyy=A(:,5);        % Quinta columna, eje y
Gzz=A(:,6);        % Sexta columna, eje z

% Los datos llegan con un poco de ruido, con lo que lo mejor
% es realizarles un filtro
Gx = medfilt1(Gxx,10);
Gy = medfilt1(Gyy,10);
Gz = medfilt1(Gzz,10);

%Si observas que el filtro tiene unos valores que no se corresponden




%Tras ello, se grafican los valores de los tres ejes
figure(2)
plot(Gx)        %Eje x
hold on
plot(Gy)        %Eje y
hold on
plot(Gz)        %Eje z
hold on
legend('Gx','Gy','Gz')
%%acc = 0.0012;
%%gyro = 0.0610;