% Se carga el archivo de texto en matlab
A = importdata('LECTURA2.txt');


% Las primeras tres columnas son datos del acelerómetro
% Vienen en raw data, por lo que hay que convertirlo
% en valores reales
Axx=A(:,1)*0.00122;    % Primera columna, eje x
Ayy=A(:,2)*0.00122;    % Segunda columna, eje y
Azz=A(:,3)*0.00122;    % Tercera columna, eje z

% Los datos llegan con un poco de ruido, con lo que lo mejor
% es realizarles un filtro
Ax = medfilt1(Axx,50);
Ay = medfilt1(Ayy,50);
Az = medfilt1(Azz,50);



%Tras ello, se grafican los valores de los tres ejes
figure(1)
plot(Ax)    %Eje x
hold on
plot(Ay)    %Eje y
hold on
plot(Az)    %Eje z
hold on
%plot(Ayy)
legend('Ax','Ay','Az')

% Ahora se hace lo mismo con el giroscopio
% Las primeras tres columnas son datos del giroscopio
% Vienen en raw data, por lo que hay que convertirlo
% en valores reales
Gxx=A(:,4)*0.0061;        % Cuarta columna, eje x
Gyy=A(:,5)*0.0061;        % Quinta columna, eje y
Gzz=A(:,6)*0.0061;        % Sexta columna, eje z

% Los datos llegan con un poco de ruido, con lo que lo mejor
% es realizarles un filtro
Gx = medfilt1(Gxx,10);
Gy = medfilt1(Gyy,10);
Gz = medfilt1(Gzz,10);

Figure(1)

%Tras ello, se grafican los valores de los tres ejes
figure(2)
plot(Gx)        %Eje x
hold on
plot(Gy)        %Eje y
hold on
plot(Gz)        %Eje z
hold on
legend('Gx','Gy','Gz')
%%acc = 0.0012;
%%gyro = 0.0610;